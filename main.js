var listGejala = [
    {
        gejala : 'Mual Dan Muntah',
        kode : 'g1',
        poin : [
            {
                kode : 'p1',
                poin : 5
            },
            {
                kode : 'p2',
                poin : 0,
            },
            {
                kode : 'p3',
                poin : 0
            }
        ],
    },
    {
        gejala : 'Berkeringat Pada Mala Hari',
        kode : 'g2',
        poin : [
            {
                kode : 'p1',
                poin : 0
            },
            {
                kode : 'p2',
                poin : 15,
            },
            {
                kode : 'p3',
                poin : 0
            }
        ],
    },
    {
        gejala : 'Demam',
        kode : 'g3',
        poin : [
            {
                kode : 'p1',
                poin : 5
            },
            {
                kode : 'p2',
                poin : 10,
            },
            {
                kode : 'p3',
                poin : 0
            }
        ],
    },
    {
        gejala : 'Kelelahan Dan Lemas',
        kode : 'g4',
        poin : [
            {
                kode : 'p1',
                poin : 5
            },
            {
                kode : 'p2',
                poin : 10,
            },
            {
                kode : 'p3',
                poin : 0
            }
        ],
    },
    {
        gejala : 'Bibir dan Kuku Pucat',
        kode : 'g5',
        poin : [
            {
                kode : 'p1',
                poin : 5
            },
            {
                kode : 'p2',
                poin : 0,
            },
            {
                kode : 'p3',
                poin : 0
            }
        ],
    },
    {
        gejala : 'Sakit Kepala',
        kode :'g6',
        poin : [
            {
                kode : 'p1',
                poin : 5
            },
            {
                kode : 'p2',
                poin : 0,
            },
            {
                kode : 'p3',
                poin : 0
            }
        ],
    },
    {
        gejala : 'Gemetar',
        kode : 'g7',
        poin : [
            {
                kode : 'p1',
                poin : 5
            },
            {
                kode : 'p2',
                poin : 0,
            },
            {
                kode : 'p3',
                poin : 0
            }
        ],
    },
    {
        gejala : 'Nyeri Sendi',
        kode : 'g8',
        poin : [
            {
                kode : 'p1',
                poin : 5
            },
            {
                kode : 'p2',
                poin : 0,
            },
            {
                kode : 'p3',
                poin : 0
            }
        ],
    },
    {
        gejala : 'Sakit Tenggorokan',
        kode : 'g9',
        poin : [
            {
                kode : 'p1',
                poin : 5
            },
            {
                kode : 'p2',
                poin : 0,
            },
            {
                kode : 'p3',
                poin : 0
            }
        ],
    },
    {
        gejala : 'Detak Jantung Cepat',
        kode : 'g10',
        poin : [
            {
                kode : 'p1',
                poin : 5
            },
            {
                kode : 'p2',
                poin : 0,
            },
            {
                kode : 'p3',
                poin : 0
            }
        ],
    },
    {
        gejala : 'Batuk',
        kode : 'g11',
        poin : [
            {
                kode : 'p1',
                poin : 5
            },
            {
                kode : 'p2',
                poin : 0,
            },
            {
                kode : 'p3',
                poin : 0
            }
        ],
    },
    {
        gejala : 'Keringat Dingin',
        kode : 'g12',
        poin : [
            {
                kode : 'p1',
                poin : 10
            },
            {
                kode : 'p2',
                poin : 0,
            },
            {
                kode : 'p3',
                poin : 0
            }
        ],
    },
    {
        gejala : 'Penurunan Berat Badan',
        kode : 'g13',
        poin : [
            {
                kode : 'p1',
                poin : 0
            },
            {
                kode : 'p2',
                poin : 15,
            },
            {
                kode : 'p3',
                poin : 20
            }
        ],
    },
    {
        gejala : 'Tidak Nafsu Makan',
        kode : 'g14',
        poin : [
            {
                kode : 'p1',
                poin : 5
            },
            {
                kode : 'p2',
                poin : 10,
            },
            {
                kode : 'p3',
                poin : 0
            }
        ],
    },
    {
        gejala : 'Nyeri Dada',
        kode : 'g15',
        poin : [
            {
                kode : 'p1',
                poin : 5
            },
            {
                kode : 'p2',
                poin : 15,
            },
            {
                kode : 'p3',
                poin : 30
            }
        ],
    },
    {
        gejala : 'Sesak Napas',
        kode : 'g16',
        poin : [
            {
                kode : 'p1',
                poin : 15
            },
            {
                kode : 'p2',
                poin : 0,
            },
            {
                kode : 'p3',
                poin : 20
            }
        ],
    },
    {
        gejala : 'Batuk Mengeluarkan Darah',
        kode : 'g17',
        poin : [
            {
                kode : 'p1',
                poin : 15
            },
            {
                kode : 'p2',
                poin : 25,
            },
            {
                kode : 'p3',
                poin : 30
            }
        ],
    }
];

var listPenyakit = [
    {
        penyakit : 'Paru - paru basah',
        kode : 'p1',
        poin : 0
    },
    {
        penyakit : 'TBC',
        kode : 'p2',
        poin : 0
    },
    {
        penyakit : 'Kanker paru-paru',
        kode : 'p3',
        poin : 0
    }
];

$(document).ready(function(){


    showList(listGejala);

    $('#check').on('click', () => {
        checkValue();
    });

});


function showList(data){
    data.forEach(el => {
        
        $('#listGejala').append(`<div class="col-lg-3 form-check mb-3">
                                    <input type="checkbox" name="gejala"  value="${el.kode}" class="form-check-input">
                                    <label class="form-check-label" for="defaultCheck1">
                                        ${el.gejala}
                                     </label>
                                </div>`
                                );
    });
}


function checkValue(){
    let data = [];
    
    $('.form-check-input:checked').each((index, item) => {
       data.push($(item).val());
    });


    let getGejala = listGejala.filter(f => data.includes(f.kode));

    let getPoin = [];

    listPenyakit.forEach(el => {
        let filterPoin = getGejala.map(o => o.poin.filter(f => f.kode == el.kode)).flat()
        let getSumPoin = filterPoin.map(o => o.poin).reduce((a, b) => a + b);                        

        getPoin.push({penyakit : el.penyakit ,kode : el.kode , poin : getSumPoin});
    });

    let maxValue = getPoin.reduce((a, b) => {
        return (a.poin > b.poin) ? a : b;
    })
    

    let validasiValue = maxValue.poin >= 75 ? maxValue : null;
    console.log(maxValue.poin, validasiValue);

    showResult(validasiValue);

    
}

function showResult(result){
    $('#result-container').addClass('d-none');
    $('#result-container').removeClass('d-none');
    $('#result').empty();
    if(result == null){
        $('#result').append(`<h1>Anda Tidak mengalami gangguan paru paru</h1>`);
    }else{
        $('#result').append(`<h1>Anda mengalami ${result.penyakit} </h1>`)
    }

}